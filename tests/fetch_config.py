#!/usr/bin/env python
import os
import pickle

if __name__ == '__main__':
    config_file = '/tmp/token.pickle'

    from google.oauth2.credentials import Credentials
    c = Credentials(token=os.environ.get('TOKEN'),
        client_id=os.environ.get('CLIENT_ID'),
        client_secret=os.environ.get('CLIENT_SECRET'),
        refresh_token=os.environ.get('REFRESH_TOKEN'),
        token_uri=os.environ.get('TOKEN_URI'))
    pickle.dump(c, open(config_file, 'wb'))
