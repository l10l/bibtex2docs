import sys
import os.path as op
import os
sys.path.append(op.join(op.split(op.dirname(__file__))[0], 'bin'))

def test_001():
    id = os.environ.get('DOCUMENT_ID')

    v = 'BIBTEXFILE'
    bib_fp = os.environ.get(v) if v in os.environ \
        else '/home/grg/My Collection.bib'
    fp = 'credentials.json'

    from bibtex2docs import replace_references, create_parser
    import bibtex2docs
    bibtex2docs.TESTS = True
    p = create_parser()
    replace_references(id, bib_fp, fp)
    replace_references(id, bib_fp, fp, highlight=False)

    #replace_references(id, bib_fp, fp, numindex=True)
