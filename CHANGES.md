Latest changes
===============

Release 0.1.2.0
---------------

      * New Features

         - Numerical mode improved (references displayed like [2-5,8,10])
         - References are highlighted in color

