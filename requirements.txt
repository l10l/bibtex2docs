google-api-python-client>=1.7.11
google-auth-httplib2>=0.0.3
google-auth-oauthlib>=0.4.0
nameparser>=1.0.4
bibtexparser>=1.1.0
nose>=1.3
coverage>=4.5
